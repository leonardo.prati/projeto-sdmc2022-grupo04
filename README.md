# Projeto SMDC 2022 - Grupo 4

Este repositório contém o projeto realizado como parte dos requisitos de conclusão da disciplina SCC0965 - Streaming de Dados, Microsserviços e Containers. Os autores são:

- Eduardo Argenti
- Leonardo Prati (8300079)

## Visão Geral do Projeto

O projeto consiste no design e na implementação de um sistema distribuído num contexto de IoT. Dentro da solução elaborada pela equipe, considerou-se um ambiente residencial e os disposivos comuns neste domínio (lâmpadas inteligentes, ar-condicionado, etc). O sistema projetado contém os seguintes componentes:
- Backend: Microsserviço Python responsável por servir e processar os dados dos dispositivos IoT (comunicação via protocolo MQTT) e as requisições do usuário para interagir com os dispositivos (ler, alterar valores, etc, implementado via protocolo HTTP).
- Frontend: Interface do usuário, na qual pode ler e gerenciar os dispositivos.
- Broker: Servidor MQTT (Eclipse Mosquitto) responsável pela troca de mensagens entre os dispositivos e o Backend.
- Armazenamento: Servidor MongoDB responsável pela persistência de dados. É manipulado exclusivamente pelo Backend.

## Executando e Interagindo com a Aplicação
Os requisitos para executar a aplicação são o Docker (testado na versão 20.10.20) e Docker Compose (testado na versão v2.12.0).

Uma vez que possua os requisitos, basta acessar o diretório do projeto e executar o comando:
```
docker compose up -d
```

O frontend estará acessivel no endereço [localhost:8501](localhost:8501).

O backend pode ser acessado através do endereço [localhost:3000](localhost:3000). A documentação das rotas disponíveis é encontrada em [localhost:3000/docs](localhost:3000/docs), incluindo uma interface para testes e a definição dos dados de entrada e saída para cada rota.

Os modelos de dados utilizados no projeto podem ser obtidos no arquivos [database.py](./backend/database.py) e [model.py](./backend/model.py), que podem ser localizado no diretório "backend". São estes arquivo que contém as modelagens e as funções de manipulação de banco de dados aplicadas na solução.

## Limitações e próximos passos
Na versão atual, o repositório conta com algumas funcionalidades mockadas. Por exemplo, o Backend se conecta com o servidor MQTT, porém, nenhum dado é trocado, Caso alguma mensagem seja enviada para o tópico "teste", será apresentada nos logs do Backend, mas nenhuma ação ocorrerá.

O Frontend também possui apenas dados mockados, não correspondendo aos dispositivos cadastrados no Backend e não conseguindo interagir com eles.

Estes comportamentos serão finalizados para a entrega final do projeto.

## Contato
Quaisquer dúvidas sobre detalhes da implementação ou da execução do projeto podem ser encaminhadas para:
leonardo.prati@usp.br ou eduardo.argenti@usp.br.
