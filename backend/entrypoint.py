import json
import paho.mqtt.client as mqtt
import uvicorn

from fastapi import FastAPI, HTTPException

from model import (
    Device,
    LightBulb,
    Sensor,
    AirConditioner
)

from database import(
    fetch_light_bulb,
    fetch_all_light_bulbs,
    create_light_bulb,
    change_light_bulb_status,
    delete_light_bulb,
    fetch_sensor,
    fetch_all_sensors,
    create_sensor,
    change_sensor_status,
    delete_sensor
)

devices = {}

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc), flush=True)
    client.subscribe("test")

def on_message(client, userdata, msg):
    content = json.loads(msg.payload.decode("utf-8"))
    devices[content["name"]] = {}
    for key, value in content.items():
        devices[content["name"]][key] = value
    print(msg.topic+" "+str(msg.payload), flush=True)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("mosquitto", 1883, 60)

client.loop_start()

app = FastAPI()
app.client = client

@app.get('/')
async def hello():
    return 'Hello World'

@app.get('/devices')
async def get_devices():
    return devices


# Get sensor
@app.get("/sensor")
async def get_sensors():
    response = await fetch_all_sensors()
    return response

@app.get("/sensor{code}", response_model=Sensor)
async def get_sensor_by_code(sensorCode):
    response = await fetch_sensor(sensorCode)
    if response:
        return response
    raise HTTPException(404, f"There is no sensor with code {sensorCode}.")

# Create sensor
@app.post("/sensor")
async def post_sensor(sensor:Sensor):
    response = await create_sensor(sensor.dict())

    raise HTTPException(response['httpCode'], response['detail'])

# Update sensor
@app.put("/sensor/{code}")
async def put_sensor(sensorCode: str, status: bool):
    response = await change_sensor_status(sensorCode, status)
    if response:
        return response
    raise HTTPException(400, "Bad request")

# Delete sensor
@app.delete("/sensor/{code}")
async def delete_sensor(code):
    response = await delete_sensor(code)
    if response:
        return response
    raise HTTPException(400, "Bad request")


if __name__ == "__main__":
    try:
        uvicorn.run("entrypoint:app", port=3000)
    except Exception as e:
        print(e, flush=True)