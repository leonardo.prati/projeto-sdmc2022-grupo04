from model import LightBulb, Sensor, AirConditioner
from datetime import datetime

# MongoDB driver
import motor.motor_asyncio

# Initializing the database
client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://db:27017')
database = client.LabDevices
collectionLightBulbs = database.lightbulbs
collectionSensors = database.sensors
collectionAirConditioners = database.airconditioners

# Functions

# Light Bulbs
async def fetch_light_bulb(code):
    document = await collectionLightBulbs.find_one({"code": code})
    return document


async def fetch_all_light_bulbs():
    lightbulbs = list()
    cursor = collectionLightBulbs.find({})
    async for document in cursor:
        lightbulbs.append(LightBulb(**document))

    return lightbulbs


async def create_light_bulb(document):
    check = await fetch_light_bulb(document['code'])

    response = dict()

    if check == None:
        document['dateAdded'] = datetime.now()
        document['dateUpdated'] = datetime.now()
        await collectionLightBulbs.insert_one(document)
        response['httpCode'] = 201
        response['detail'] = 'Light bulb added successfully!'
    else:
        response['httpCode'] = 409
        response['detail'] = 'Conflict: light bulb already exists in database!'

    return response


async def change_light_bulb_status(code, status):
    check = await fetch_light_bulb(code)
    response = dict()

    if check == None:
        response['httpcode'] = 404
        response['detail'] = f"Light bulb code {code} not found"
    else:
        newStatus = 'on' if status == 1 else 'off'

        await collectionLightBulbs.update_one({"status": status})
        await collectionLightBulbs.update_one({"dateUpdated": datetime.now()})

        response['httpCode'] = 202
        response['detail'] = f"Light bulb {code} is {newStatus}!"

    return response


async def delete_light_bulb(code):
    check = await fetch_light_bulb(code)
    response = dict()

    if check == None:
        response['httpcode'] = 404
        response['detail'] = f"Light bulb code {code} not found"
    else:
        await collectionLightBulbs.delete_one({"code": code})

        response['httpCode'] = 202
        response['detail'] = f"Successfully deleted light bulb {code}!"

    return response


# Sensors
async def fetch_sensor(code):
    document = await collectionSensors.find_one({"code": code})
    return document


async def fetch_all_sensors():
    sensors = list()
    cursor = collectionSensors.find({})
    async for document in cursor:
        sensors.append(Sensor(**document))

    return sensors


async def create_sensor(document):
    check = await fetch_sensor(document['code'])

    response = dict()

    if check == None:
        document['dateAdded'] = datetime.now()
        document['dateUpdated'] = datetime.now()
        await collectionSensors.insert_one(document)
        response['httpCode'] = 201
        response['detail'] = 'Sensor added successfully!'
    else:
        response['httpCode'] = 409
        response['detail'] = 'Conflict: sensor already exists in database!'

    return response


async def change_sensor_status(code, status):
    check = await fetch_sensor(code)
    response = dict()

    if check == None:
        response['httpcode'] = 404
        response['detail'] = f"Sensor {code} not found"
    else:
        newStatus = 'on' if status == 1 else 'off'

        await collectionSensors.update_one({"code": code }, {"$set": {"status": status}})
        await collectionSensors.update_one({"code": code }, {"$set": {"dateUpdated": datetime.now()}})

        response['httpCode'] = 202
        response['detail'] = f"Sensor {code} is {newStatus}!"

    return response


async def delete_sensor(code):
    check = await fetch_sensor(code)
    response = dict()

    if check == None:
        response['httpcode'] = 404
        response['detail'] = f"Sensor {code} not found"
    else:
        await collectionSensors.delete_one({"code": code})

        response['httpCode'] = 202
        response['detail'] = f"Successfully deleted sensor {code}!"

    return response
