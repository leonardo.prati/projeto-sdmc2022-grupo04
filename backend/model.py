from pydantic import BaseModel

class Device(BaseModel):
    code: str
    status: bool

class LightBulb(Device):
    colorCode: str

class Sensor(Device):
    roomTemperature: float

class AirConditioner(Device):
    targetTemperature: float