import streamlit as st

from components import LightBulb

if 'Luz 1' not in st.session_state:
    bulb1 = LightBulb('1', True, '#FFFFFF', 50)
    st.session_state['Luz 1'] = bulb1
else:
    bulb1 = st.session_state['Luz 1']
    
if 'Luz 2' not in st.session_state:
    bulb2 = LightBulb('2', True, '#FFFFFF', 50)
    st.session_state['Luz 2'] = bulb2
else:
    bulb2 = st.session_state['Luz 2']

if __name__ == "__main__":    
    st.title("Dashboard")

    expander = st.expander("Sala de Estar", expanded=True)
    col = expander.columns((1,2,1,2,1), gap="small")
    bulb1.render(col[1])
    bulb2.render(col[3])
