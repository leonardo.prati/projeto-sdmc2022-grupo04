import streamlit as st


class Device:
    def __init__(self, code: str, status: bool):
        self.code = code
        self.status = status

class LightBulb(Device):
    def __init__(self, code: str, status: bool, color_code: str, luminosity: int):
        super().__init__(code, status)
        self.color_code = color_code
        self.luminosity = luminosity

    def render(self, parent_component: st):
        parent_component.subheader(f"{self._get_key_sufix()}")
        color_code = parent_component.color_picker('Cor da Luz', value=self.color_code, key=f" Cor {self._get_key_sufix()}")
        print(color_code)
        self._update_color_code(color_code)
        new_luminosity = parent_component.slider('Luminosidade', min_value=0, max_value=100, value=self.luminosity, step=1, key=f"Luminosidade {self._get_key_sufix()}")
        print(new_luminosity)
        self._update_luminosity(new_luminosity)
        new_status = parent_component.select_slider('Status', options=['Desligado', 'Ligado'], value=self._get_status(), key=f"Status {self._get_key_sufix()}")
        print(new_status)
        self._update_status(new_status)
        parent_component.image(self._get_icon(), use_column_width=True)
        
    def _get_key_sufix(self):
        return f"Luz {self.code}"

    def _get_icon(self):
        if self.status:
            return './assets/true_lightbulb.png'
        else:
            return './assets/false_lightbulb.png'
    
    def _get_status(self):
        if self.status:
            return 'Ligado'
        else:
            return 'Desligado'
    
    def _update_color_code(self, new_color_code):
        self.color_code = new_color_code

    def _update_luminosity(self, new_luminosity):
        self.luminosity = new_luminosity
    
    def _update_status(self, new_status):
        if new_status == 'Ligado':
            self.status = True
        else:
            self.status = False

    def __repr__(self) -> str:
        return f"LightBulb(code={self.code}, status={self.status}, color_code={self.color_code}, luminosity={self.luminosity})"
    